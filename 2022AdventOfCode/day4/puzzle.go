package main
import ("os"
	"fmt"
	"bufio"
	"strconv"
	"strings"
)

func main(){
	in := bufio.NewScanner(os.Stdin)
	var score int64 = 0
	var score2 int64 = 0
//	var input []string;

	for in.Scan(){
		//extract the data
		jobs := strings.Split(in.Text(), ",")
		job1 := strings.Split(jobs[0], "-")
		job2 := strings.Split(jobs[1], "-")
		//convert data
		j11, _ := strconv.ParseInt(job1[0], 10, 32)
		j12, _ := strconv.ParseInt(job1[1], 10, 32)
		j21, _ := strconv.ParseInt(job2[0], 10, 32)
		j22, _ := strconv.ParseInt(job2[1], 10, 32)

		fmt.Println(j11, j12, j21, j22)
		//look if fully overlapped
		var overlaps int64 = 0;
		for i := j11; i <= j12; i++{
			
			for ii := j21; ii <= j22; ii++{
				if i == ii{
					overlaps++;
				}
			}
		}
		if overlaps-1 == j12-j11{
			score++
		}else if overlaps-1 == j22-j21{
			score++
		}
		if overlaps > 0{
			score2++
		}
		fmt.Println(overlaps, j12-j11, j22-j21)
	}
	fmt.Println(score)
	fmt.Println(score2)
}