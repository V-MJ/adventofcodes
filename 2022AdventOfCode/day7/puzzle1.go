package main
import ("os"
	"fmt"
	"bufio"
	"strconv"
	"strings"
)

type file struct{
	size int;
	name string;
}
type directory struct{
	files []file;
	directories []directory;
	parrent *directory;
	name string;
	contains int;//contains x much of data
}

func calcsizes(root *directory){
	var contains int = 0;
	//count files in dictionary
	for i := 0; i < len(root.files); i++{
		contains += root.files[i].size
	}
	//recurse to lower folders
	for i := 0; i < len(root.directories); i++{
		calcsizes(&root.directories[i])
	}
	//add results from folders
	for i := 0; i < len(root.directories); i++{
		contains += root.directories[i].contains
	}
	//set contains
	root.contains = contains;
}

func part1(root *directory)int{
	retval := 0;
	for i := 0; i < len(root.directories); i++{
		if root.directories[i].contains <= 100000{
			retval += root.directories[i].contains
		}
		retval += part1(&root.directories[i])
	}
	return retval;
}

func part2(root *directory)[]int{
	var tmp []int;
	tmp = append(tmp, root.contains);
	for i := 0; i < len(root.directories); i++{
		tmp = append(tmp, part2(&root.directories[i])...)
	}
	return tmp;
}

func main(){
	in := bufio.NewScanner(os.Stdin)
	//var score int = 0
	
	var log []string;
	//get the log
	for in.Scan(){
		log = append(log, in.Text())
	}

	var root directory
	root.parrent	= nil;
	root.name	= "/";
	var activeDir *directory;// = root;

	//process log into tree
	for i := 0; i < len(log); i++{
		//split line by ' '
		splt := strings.Split(log[i], " ")
		

		switch splt[0] {
			case "$":
				//if it's a cd command do stuff
				//ignore ls
				if splt[1] == "cd"{
					if splt[2] == "/"{
						activeDir = &root;
					}else if splt[2] == ".."{
						activeDir = activeDir.parrent
					} else {
						for di := 0; di < len(activeDir.directories); di++{
							if activeDir.directories[di].name == splt[2]{
								activeDir = &activeDir.directories[di]
								break;
							}
						}
					}
				}
				break;
			case "dir":
				//append a directory to directories
				tmpDir := directory{
					parrent:activeDir,
					name:	splt[1],
				};
				activeDir.directories = append(activeDir.directories, tmpDir)
				break;
			default://number aka file
				num, _ := strconv.Atoi(splt[0])
				tmpFile := file{
					size:	num,
					name:	splt[1],
				};
				//append file to active directory
				activeDir.files = append(activeDir.files, tmpFile);
				break;
		}

		fmt.Println(log[i])
		fmt.Println(activeDir)
		//fmt.Println(&root)
		fmt.Println()
	}

	//calculate folder sizes
	calcsizes(&root)
	fmt.Println(root)

	//print part1
	fmt.Println(part1(&root))
	
	//process part2
	freeSpace := 70000000 - root.contains;
	targetFree := 30000000;
	foldersSizes := part2(&root)
	var answer2 int = 70000000;
	for _, n := range foldersSizes{
		if (freeSpace+n) >= targetFree{
			if (freeSpace+n) < (freeSpace+answer2){
				answer2 = n;
			}
		}
	}
	fmt.Println(answer2)
}