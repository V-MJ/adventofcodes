package main
import ("os"
	"fmt"
	"bufio"
	"strconv"
	"strings"
)

func main(){
	in := bufio.NewScanner(os.Stdin)
	var stacks []string;
	var stack [][]byte;
	//get the stacks
	for in.Scan(){
		if in.Text()[1] == 49{
			break;
		}
		stacks = append(stacks, in.Text())
	}
	fmt.Println(stacks)
	stack = make([][]byte, 9)
	for i := len(stacks)-1; i >= 0; i--{
		ti := 0;
		for ii := 1; ii < len(stacks[i]); ii += 4{
			if stacks[i][ii] != 32{
				stack[ti] = append(stack[ti], stacks[i][ii])
			}
			ti++;
		}
		ti = 0;
	}
	fmt.Println(stack)
	

	//process on stacks
	for in.Scan(){
		if len(in.Text()) == 0 { continue; }
		ops := strings.Split(in.Text(), " ")
		fmt.Println(ops)
		times, _ := strconv.Atoi(ops[1])
		pop, _ := strconv.Atoi(ops[3])
		pop--;
		push, _ := strconv.Atoi(ops[5])
		push--;
		for i := 0; i < times; i++{
			//pop from stack1
			offs := len(stack[pop]) - 1;		//lenght of slice
			val := stack[pop][offs];
			stack[pop] = stack[pop][:offs]

			//push to stack2
			stack[push] = append(stack[push], val)

			fmt.Println(offs, val, pop, push)
		}
		fmt.Println(stack)
	}
	for i := 0; i < len(stack); i++{
		if len(stack[i]) != 0{
			fmt.Print(stack[i][len(stack[i])-1], " ")
		}
	}
	fmt.Println("\nuse ascii table to decode the above answer")
}