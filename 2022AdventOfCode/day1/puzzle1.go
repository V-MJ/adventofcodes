package main
import ("os"
	"fmt"
	"bufio"
	"strconv"
)

func main(){
	var calories []int64;

	//go throuh stdin and fill list with values
	in := bufio.NewScanner(os.Stdin)
	var calory int64 = 0;
	for in.Scan(){
		if in.Text() == ""{
			calories=append(calories, calory)
			calory=0
		}else{
			tmpcalory, _ := strconv.ParseInt(in.Text(), 10, 64)
			calory += tmpcalory
		}
	}
	//fmt.Println(calories)
	//find lowest value
	highestValue := [3]int64{-1, -1, -1};
	for cal := range calories{
		//find lowest value index
		lv := 0;
		for i := range highestValue{
			if highestValue[i] <= highestValue[lv]{
				lv=i
			}
		}
		//replace lowest value
		if calories[cal] > highestValue[lv] {
			highestValue[lv] = calories[cal]
		}
	}


	//printing
	hv := 0;
	for i := range highestValue{
		if highestValue[i] >= highestValue[hv]{
			hv=i
		}
	}
	fmt.Println("highestValue:", highestValue[hv])
	fmt.Println("Sum of top 3 highestValues:", highestValue[0]+highestValue[1]+highestValue[2])
	fmt.Println("top 3 highestValues:", highestValue)
}