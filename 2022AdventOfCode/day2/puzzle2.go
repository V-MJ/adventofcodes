package main
import ("os"
	"fmt"
	"bufio"
	"strings"
)

func main(){
	in := bufio.NewScanner(os.Stdin)
	var score int64 = 0
	for in.Scan(){
		ops := strings.Split(in.Text(), " ")
		switch (ops[1][0]-88){
			case 0:
				score += 0;//loss
				score += int64(((ops[0][0]-65 + 2) % 3) + 1);
				break;
			case 1:
				score += 3;//draw
				score += int64(ops[0][0]-65 + 1);
				break;
			case 2:
				score += 6;//win
				score += int64(((ops[0][0]-65 + 1) % 3) + 1);
				break;
		}
	}
	fmt.Println(score)
}