#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>

int file[1024],  PC;
bool Halt;

//stop the program
int halt(int state){
	switch (state){
		case 0 :
			printf("Program halted self.\n");
			break;
		default :
			printf("Invalid optcode!\n");
	}
	Halt = true;
}

int main(int argc, char *argv[]){

	//make stdin into a array of ints
	int iteration;
	#pragma omp simd
	for(iteration = 1; iteration < argc; iteration++){
		file[iteration-1] = atoi(argv[iteration]);
	}
	//modify the input data
	while(Halt == false){
		switch (file[PC]){
			case 1 :
				file[file[PC+3]] = file[file[PC+1]]+file[file[PC+2]];
				break;
			case 2 :
				file[file[PC+3]] = file[file[PC+1]]*file[file[PC+2]];
				break;
			case 99 :
				halt(0);
				break;
			default :
				halt(1);
		};
		PC += 4; //increment Program Counter
	}
	//print the final output
	printf("\n");
	int ln;
	for (ln = 0; ln < argc-1; ln++){
		printf("%d,", file[ln]);
	}
	printf("\n");
}
