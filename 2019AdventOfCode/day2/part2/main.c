#include <stdio.h>
#include <stdlib.h>

int file[1024];
int PC,  Halt;

int main(int argc, char *argv[]){

	//make stdin into a array of ints
	int iteration;
	#pragma omp simd
	for(iteration = 1; iteration < argc; iteration++){
		file[iteration-1] = atoi(argv[iteration]);
	}
	//modify the input data
	while(Halt == 0){
		switch (file[PC]){
			case 1 :
				file[file[PC+3]] = file[file[PC+1]]+file[file[PC+2]];
				break;
			case 2 :
				file[file[PC+3]] = file[file[PC+1]]*file[file[PC+2]];
				break;
			case 99 :
				Halt = 1;
				break;
			default :
				Halt = 1;
		};
		PC += + 4;
	}
	//print the final output
	printf("\n%d\n", file[0]);
}
