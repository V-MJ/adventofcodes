#/bin/sh

rm partfuels.txt
LINE=`cat input.txt`
for LINES in $LINE; do
#echo "$LINES" >> partfuels.txt
VAR="$LINES";
	while [ "$VAR" -ge 0 ]
	do
		echo "scale = 0; $VAR/3-2" |bc >> partfuels.txt
		VAR=`tail -1 partfuels.txt` 
	done
done

#remove anything like -1 or -9 and calculate the sum
cat partfuels.txt |sed 's/-[0-9]/0/g' |awk '{sum+=$1} END {print sum}'
