#/bin/sh

rm partfuels.txt
LINE=`cat input.txt`
for LINES in $LINE; do
	echo "scale = 0; $LINES/3-2" |bc >> partfuels.txt
done


#calculate the fuel sum
cat partfuels.txt |awk '{sum+=$1} END {print sum}'
