#/bin/sh
XMIN="$1"
XMAX="$2"

rising(){
	 Y=`echo "$XMIN / 1 % 10" | bc`
	 K=`echo "$XMIN / 10 % 10" | bc`
	 S=`echo "$XMIN / 100 % 10" | bc`
	 YT=`echo "$XMIN / 1000 % 10" | bc`
	KT=`echo "$XMIN / 10000 % 10" | bc`
	ST=`echo "$XMIN / 100000 % 10" | bc`
	FD1=`echo "$XMIN / 1 % 10" | bc`
	FD2=`echo "$XMIN / 10 % 10" | bc`
	SD1=`echo "$XMIN / 100 % 10" | bc`
	SD2=`echo "$XMIN / 1000 % 10" | bc`
	LD1=`echo "$XMIN / 10000 % 10" | bc`
	LD2=`echo "$XMIN / 100000 % 10" | bc`
	[ "$KT" -ge "$ST" ] && [ "$YT" -ge "$KT" ] && [ "$S" -ge "$YT" ] && [ "$K" -ge "$S" ] && [ "$Y" -ge "$K" ] &&     [ "$FD1" -eq "$FD2" ] && [ "$SD1" -eq "$SD2" ] && [ "$LD1" -eq "$LD2" ] && echo "$XMIN"	
}

while [ "$XMIN" -lt "$XMAX" ]
do
	#echo "$XMIN"
	rising
	XMIN=`expr "$XMIN" + 1`
done
rising
#echo "$XMIN"

#dash solver.sh | egrep '(\w)\1' | nl | tail -1
