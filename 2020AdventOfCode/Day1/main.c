#include <stdio.h>


int main(int argc, char *argv[]){
	//the main thing
	int arr[200];

	//scanf to a array
	for(int i = 0; i < 200; i++){
		scanf("%d", &arr[i]);
	}

	//test print
//	for(int ti = 0; ti < 200; ti++){
//		printf("%d\n", arr[ti]);
//	}

	//part1 solver
	printf("\nSolving part 1\n");
	for(int ei = 0; ei < 200; ei++){
	for(int ii = 0; ii < 200; ii++){
		if(arr[ii]+arr[ei] == 2020){
			printf("%d\n", arr[ii]*arr[ei]);
			ii += 512;
			ei += 512;
		}
	}}	

	printf("\n\nSolving part 2\n");
	for(int ei = 0; ei < 200; ei++){
	for(int mi = 0; mi < 200; mi++){
	for(int ii = 0; ii < 200; ii++){
		if(arr[ii]+arr[mi]+arr[ei] == 2020){
			printf("%d\n", arr[ii]*arr[mi]*arr[ei]);
			ii += 512;
			mi += 512;
			ei += 512;
		}
	}}}

}
