#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

int main(int argc, char *argv[]){
	
	int acpass = 0;

	for(int i = 0; i < 1000; i++){
		int min;
		int max;
		char chr;
		char psswd[255];
		//get the min num, switch and password
		scanf("%d %d %c %s", &min, &max, &chr, psswd);
		printf("%d %d %c %s", min, max, chr, psswd);

		//analyse passwd
		int ln=0;
		int are=0;
		while( psswd[ln] != '\0' ){
			if(psswd[ln] == chr){
				are++;
		}
			ln++;
		}
//431

		if(are >= min && are <= max ){
			acpass++;
		}
		printf(" %d %d %d\n", min, max, are);
	}
	printf("%d\n", acpass);
}
