#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
//some variables
char map[323*32];

int chits(int x, int y){
	int yi = 0;
	int xi = 0;
	int hit = 0;
	while( yi < 323 ){
		if(map[xi + yi * 32] == '#' ){
			hit++;
		}
		printf("%c", map[yi * 32 + xi]);
		yi += y;
		xi += x;
	if( xi >= 31){
		xi -= 31;
	}
	}
	printf("\n\n");
	return hit;
}

int main(int argc, char *argv[]){

	//parse stdin
	read(STDIN_FILENO, map, 323*32);
//	printf( "\n" );

//	for(int i = 0; i < 323*32; i++){
//		printf("%c", map[i]);
//	}

//0x0xx0xx00xx0xxx0xxx0x0x0x0xx0
	//part1
//	int yi = 0;
//	int xi = 0;
//	int x = 3;
//	int y = 1;
//	int hit = 0;
//	while( yi < 323 ){
//		if(map[xi + yi * 32] == '#' ){
//			hit++;
//		}
//		printf("%c", map[yi * 32 + xi]);
//		yi += y;
//		xi += x;
//	if( xi >= 31){
//		xi -= 31;
//	}
//		xi = xi % 31;
//	}

	//part2

//    Right 1, down 1.
//    Right 3, down 1. (This is the slope you already checked.)
//    Right 5, down 1.
//    Right 7, down 1.
//    Right 1, down 2.
	int o11o = chits(1, 1);
	int o31o = chits(3, 1);
	int o51o = chits(5, 1);
	int o71o = chits(7, 1);
	int o12o = chits(1, 2);
	int mult = o11o * o31o * o51o * o71o * o12o;

	printf("\n1 1 %d\n", o11o);
	printf("\n1 1 %d\n", o31o);
	printf("\n1 1 %d\n", o51o);
	printf("\n1 1 %d\n", o71o);
	printf("\n1 1 %d\n", o12o);
	printf("\nmlt %d\n", mult);
}
