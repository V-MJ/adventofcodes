#include <data_struchures.h>

//linked list data

lnode *create_linkedlist(int value){
        llnode *ret = malloc(sizeof(llnode));
        ret->value = value;
        ret->next = NULL;
        return ret;
}
llnode *llpush(llnode *head, int value){
        llnode *tmp = createnode(value);
        tmp->next = head;
        return tmp;

}
llnode *llpop( llnode *head ){
        llnode *tmp = head->next;
        free(head);
        return tmp;
}

crnode *create_circularbuffer( int value ){
	crnode *tmp = malloc(sizeof(crnode));
	tmp->value = value;
	tmp->next = tmp;
	tmp->prev = tmp;
	return tmp;
}

crnode *crpush( crnode *point, int value ){
	crnode *tmp = point;
	crnode *tnp = point->next;
	crnode *new = createbuffer( value );
	
	//insert the new node
	tmp->next = new;
	new->prev = tmp;
	tnp->prev = new;
	new->next = tnp;
	return new;
}

crnode *crpop( crnode *point ){
	crnode *pr = point->prev;
	crnode *nx = point->next;
	pr->next = nx;
	nx->prev = pr;
	free(point);
	return nx;
}
