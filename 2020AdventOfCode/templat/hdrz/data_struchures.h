#ifndef DATA_STRUCHURES_H
#define DATA_STRUCHURES_H
#include <stdlib.h>


//sequense datatypes
//begin sequense
	// begin linkedlist
	struct linkednode {
	        int value;
	        struct linkednode *next;
	};
	typedef struct linkednode llnode;
	llnode *create_linkelist(int value);
	llnode *llpush(llnode *head, int value);
	llnode *llpop( llnode *head );
	//llnode *llsort( llnode *head);
	// end linkedlist

	//begin circular buffer
	struct circularnode{
	        int value;
	        struct circularnode *next;
	        struct circularnode *prev;
	};
	typedef struct circularnode crnode;
	crnode *create_circularbuffer( int value );
	crnode *crpush( crnode *point, int value );
	crnode *crpop( crnode *point );
	//crnode *crsort( crnode *point );
	//end circular buffer
//end of sequense
	
//begin sets
	//begin map
//	struct mapnode{//mapnodes are the only sets you need
//		int key;
//		struct linkednode *values;
//		struct mapnode next;
//	};
//	typedef struct mapnode map;
//	map *createmap( int key );
//	map mapvalueadd( map *mps, int key, int value );
//	map mapnodeadd( map *mps, int key );
//	map *mapfindkey( map *mps, int key );
//	map *mapfindvalue( map *mps, int value );
	//map mapsortkey( map *mps );
//	map *mapkeydiff( int ket0, int key1 );	//different valuse
//	map *mapkeysame( int key0, int key1 );	//same values
//	map *mapkeyunion( int key0, int key1 );	//combination of sets
//	int mapkeysubset( int key0, int key1 );	//is key1 subset of key 0
//	map *mapremovekey( map *mps, int key );	//remove a key
//	map *mapremove( map *mps);		//remove map
	//end map
//end of sets

#endif
