#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <math.h>
#include <unistd.h>

//linked list and related functions for good mesure
struct listnode{
	int data;
	struct listnode *next;
};
typedef struct listnode node;
node *head = NULL;
node *tmp;

void printlist( node *head ){
	node *tmp = head;

	while(tmp != NULL){
		printf("%d – ", tmp->data);
		tmp = tmp->next;
	}
	printf("\n");
}
node *mknode(int data){
	node *result = malloc(sizeof(node));
	result->data = data;
	result->next = NULL;
	return result;
}
node *push(node **head, node *node_to_insert){
	node_to_insert->next = *head;
	*head = node_to_insert;
	return node_to_insert;
}
node *pop(node *head){
	head = head->next;
	return head;
}


//some variables
int x, y, z, w;
int arr[255];

//shared funcs of parts 1 and 2
int a(){
}

//the "main" functions
void part1(){

}
void part2(){

}

int main(int argc, char *argv[]){
	//parse launch options

	//parse stdin
	char cfsi;
	while(read(STDIN_FILENO, &cfsi, 1) > 0){
		//now do stuff with char @ cfsi
		printf("%c", cfsi);
	}
	printf( "\n" );



	//do the thing
	printf( "Doing part1\n" );
	part1();
	printf( "Part1 Done\n\n" );

	printf( "Doing part2\n" );
	part2();
	printf( "Part2 Done\n" );
}
