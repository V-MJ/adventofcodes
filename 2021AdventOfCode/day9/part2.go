package main
import ("fmt"
	"os"
	"bufio"
)
var input [][]byte;
func main(){
	//get and split the input
	scanner := bufio.NewScanner(os.Stdin);
	iter := 0;
	for scanner.Scan(){
		slice := []byte(scanner.Text());
		slice = slice[:len(slice)-1]
		input = append(input, slice);
		iter++;
	}
	xLen := len(input[0]);
	yLen := len(input);
	var lowestPoints []int64;
	var areas []int64;
	for i := 0; i < yLen; i++{
	for ii := 0; ii < xLen; ii++{
		var surroundingPoints []int64;
		curentPoint := int64(input[i][ii]-48);
		if ii-1 >= 0{//left
			surroundingPoints = append(surroundingPoints, int64(input[i][ii-1]-48));
		}
		if ii+1 < xLen{//right
			surroundingPoints = append(surroundingPoints, int64(input[i][ii+1]-48));
		}
		if i-1 >= 0{//up
			surroundingPoints = append(surroundingPoints, int64(input[i-1][ii]-48));
		}
		if i+1 < yLen{//down
			surroundingPoints = append(surroundingPoints, int64(input[i+1][ii]-48));
		}
		var higer int64;
		for pp := range surroundingPoints{
			if curentPoint < surroundingPoints[pp]{
				higer++;
			}
		}
		if higer == int64(len(surroundingPoints)){
			lowestPoints = append(lowestPoints, curentPoint);
			//lets flood fill
			floodfill(int64(i), int64(ii), int64((10+len(lowestPoints))), int64(xLen), int64(yLen));
			//now lets count the stuff
			var nCount int64;
			for yi := 0; yi < yLen; yi++{
			for xi := 0; xi < xLen; xi++{
				if int64(input[yi][xi]) == int64((10+len(lowestPoints))){
					nCount++;
				}
			}}
			fmt.Println(nCount);
			areas = append(areas, nCount);
			fmt.Println(areas, lowestPoints, nCount);
		}
	}}
	var danger int64;
	for i := range lowestPoints {
		danger += lowestPoints[i]+1;
	}
	fmt.Println("part 1:", lowestPoints, xLen, yLen, danger);
}






func floodfill(x int64, y int64, n int64, lnx int64, lny int64){
	//is 9
	if int64(input[y][x]) == 9{
		return;
	}
	//is n
	if int64(input[y][x]) == n{
		return;
	}
	//set as n
	input[y][x] = byte(n);
	//do top
	if y-1 >= 0{
		floodfill(x, y-1, n, lnx, lny);
	}
	//do bottm
	if y+1 < lny{
		floodfill(x, y+1, n, lnx, lny);
	}
	//do left
	if x-1 >= 0{
		floodfill(x-1, y, n, lnx, lny);
	}
	//do right
	if x+1 < lnx{
		floodfill(x+1, y, n, lnx, lny);
	}
}