package main
import ("fmt"
	"os"
	"bufio"
)

func main(){
	//get and split the input
	scanner := bufio.NewScanner(os.Stdin);
	var input []string;
	for scanner.Scan(){
		input = append(input, scanner.Text());
	}
	xLen := len(input[0]);
	yLen := len(input);
	var lowestPoints []int64;
	for i := 0; i < yLen; i++{
	for ii := 0; ii < xLen; ii++{
		var surroundingPoints []int64;
		curentPoint := int64(input[i][ii]-48);
		if ii-1 >= 0{//left
			surroundingPoints = append(surroundingPoints, int64(input[i][ii-1]-48));
		}
		if ii+1 < xLen{//right
			surroundingPoints = append(surroundingPoints, int64(input[i][ii+1]-48));
		}
		if i-1 >= 0{//up
			surroundingPoints = append(surroundingPoints, int64(input[i-1][ii]-48));
		}
		if i+1 < yLen{//down
			surroundingPoints = append(surroundingPoints, int64(input[i+1][ii]-48));
		}
		var higer int64;
		for pp := range surroundingPoints{
			if curentPoint < surroundingPoints[pp]{
				higer++;
			}
		}
		if higer == int64(len(surroundingPoints)){
			lowestPoints = append(lowestPoints, curentPoint);
		}
	}}
	var danger int64;
	for i := range lowestPoints {
		danger += lowestPoints[i]+1;
	}
	fmt.Println(lowestPoints, xLen, yLen, danger);
}