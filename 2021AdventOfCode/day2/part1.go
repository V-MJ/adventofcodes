package main
import ("fmt"
	"os"
	"bufio"
)

func main(){
	var forward int;
	var depth int;
	
	scanner := bufio.NewScanner(os.Stdin)
	for scanner.Scan() {
		str := scanner.Text();
		op := str[0];
		var num int = int(str[len(str)-1]) - 48;

		switch(op){
			case 'f':
				forward += num;
				break;
			case 'u':
				depth -= num;
				break;
			case 'd':
				depth += num;
				break;
		}
		op = '0'
		num = 0;
	}
	fmt.Println(forward*depth);
}