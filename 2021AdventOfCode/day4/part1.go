package main

import ("fmt"
	"os"
	"bufio"
	"strconv"
	"strings"
)

type board struct {
	marks [5][5]bool;
	nums [5][5]int;
	win bool;
}

func main(){
	//usefull variables
	scanner := bufio.NewScanner(os.Stdin)
	var bingoNums []int;
	var bingoBoards []board;
	var victoryBoard int;
	var victoryNumber int;

	//get the bingo numbers and put them in a slice
	scanner.Scan();
	csv := scanner.Text();
	scsv := strings.Split(csv,",");
	//var bingoNums []int;
	for numstr := range scsv {
		tempNum, _ := strconv.ParseInt(scsv[numstr], 10, 32);
		bingoNums = append(bingoNums, int(tempNum));
	}
	fmt.Println(bingoNums);
	scanner.Scan();	//get a empty line out from our red in buffer

	//get the boards
	var tmpBoard board;
	var lineIter int;
	for scanner.Scan() {
		str := scanner.Text();
		if (len(scanner.Text()) == 0){
			bingoBoards = append(bingoBoards, tmpBoard);
			lineIter = 0;
			continue;
		}
		snums := strings.Fields(str);
		for ln := range snums{
//			fmt.Println(ln, lineIter, tmpBoard);
			tempNum, _ := strconv.ParseInt(snums[ln], 10, 32);
			tmpBoard.nums[lineIter][ln] = int(tempNum);
		}
		lineIter++;
	}
//	fmt.Println(bingoBoards);

	//loop until victory
	for bingoNum := range bingoNums {
		var bingo bool;
		//mark the boards
		for boardp := range bingoBoards{
			for x := 0; x < 5; x++{
			for y := 0; y < 5; y++{
				if bingoBoards[boardp].nums[x][y] == bingoNums[bingoNum]{
					bingoBoards[boardp].marks[x][y] = true;
				}	
			}}
		}
		
		//x axel bingos
		for boardp := range bingoBoards{
			for x := 0; x < 5; x++{
				var hits int;
				for y := 0; y < 5; y++{
					if bingoBoards[boardp].marks[x][y] == true{
						hits++;
					}
				}
				if hits == 5{
					victoryBoard = boardp;
					victoryNumber = bingoNums[bingoNum];
					bingo = true;
				}
			}
		}
		//y axel bingos
		for boardp := range bingoBoards{
			for x := 0; x < 5; x++{
				var hits int = 0;
				for y := 0; y < 5; y++{
					if bingoBoards[boardp].marks[y][x] == true{
						hits++;
					}
				}
				if hits == 5{
					victoryBoard = boardp;
					victoryNumber = bingoNums[bingoNum];
					bingo = true;
				}
			}
		}
		//exit on bingo
		if bingo == true{
			break;
		}
	}
	fmt.Println(bingoBoards);


	//calculate the score
	var unmarkedSum int = 0;
	for x := 0; x < 5; x++{
	for y := 0; y < 5; y++{
		if bingoBoards[victoryBoard].marks[x][y] == false{
			unmarkedSum += bingoBoards[victoryBoard].nums[x][y];
		}
	}}

	fmt.Println("\n", victoryBoard, victoryNumber, unmarkedSum, unmarkedSum * victoryNumber, "\n", bingoBoards[victoryNumber]);
}