package main

import ("fmt"
	"os"
	"bufio"
	"strconv"
	"strings"
	"image"
	"image/color"
	"image/png"
	"log"
)

type line struct {
	x1 int64;
	y1 int64;
	x2 int64;
	y2 int64;
}
var allLines []line;
var udLines []line;
var lrLines []line
var diagLines []line;

func main(){
	//usefull variables
	scanner := bufio.NewScanner(os.Stdin);
	for scanner.Scan() {
		//parse input
		str := scanner.Text();
		str = strings.Replace(str, " -> ", ",", -1)
		strs := strings.Split(str,",");

		//get the lines
		var tmpLine line;
		
		tmpLine.x1, _ = strconv.ParseInt(strs[0], 10, 32);
		tmpLine.y1, _ = strconv.ParseInt(strs[1], 10, 32);
		tmpLine.x2, _ = strconv.ParseInt(strs[2], 10, 32);
		tmpLine.y2, _ = strconv.ParseInt(strs[3], 10, 32);
		allLines = append(allLines, tmpLine);
	}
	//fmt.Println(allLines);
	//make list of useable lines lines
	for lineNum := range allLines {
		if allLines[lineNum].x1 == allLines[lineNum].x2{
			udLines = append(udLines, allLines[lineNum]);
			continue;
		}
		if allLines[lineNum].y1 == allLines[lineNum].y2{
			lrLines = append(lrLines, allLines[lineNum]);
			continue;
		}
		diagLines = append(diagLines, allLines[lineNum]);
	}
	//draw lines to graph
	var graph [1000][1000] int;
	for cLine := range udLines{
		startY := udLines[cLine].y1;
		lenL := udLines[cLine].y2 - udLines[cLine].y1;
		//draw lines to graph
		if udLines[cLine].y1 > udLines[cLine].y2 {
			startY = udLines[cLine].y2;
			lenL = udLines[cLine].y1 - udLines[cLine].y2;
		}
		for y := startY; y != startY+lenL+1; y++ {
			graph[y][udLines[cLine].x1]++;
		}
	}
	
	for cLine := range lrLines{
		startY :=lrLines[cLine].x1;
		lenL := lrLines[cLine].x2 - lrLines[cLine].x1;
		//draw lines to graph
		if lrLines[cLine].x1 > lrLines[cLine].x2 {
			startY = lrLines[cLine].x2;
			lenL = lrLines[cLine].x1 - lrLines[cLine].x2;
		}
		for y := startY; y != startY+lenL+1; y++ {
			graph[lrLines[cLine].y1][y]++;
		}
	}

	for cLine := range diagLines{
		startX := diagLines[cLine].x1;
		startY := diagLines[cLine].y1;
		endX := diagLines[cLine].x2;
		endY := diagLines[cLine].y2;
		xMod := int64(1);
		yMod := int64(1);
		if diagLines[cLine].x1 > diagLines[cLine].x2{
			xMod = -1;
		}
		if diagLines[cLine].y1 > diagLines[cLine].y2{
			yMod = -1;
		}
		for startX = startX; startX != endX; startX += xMod{
			graph[startY][startX]++;
			startY += yMod;
		}
		graph[endY][endX]++;
	}



	//get the intersections
	var intersections int;
	for x := 0; x < 1000; x++{
	for y := 0; y < 1000; y++{
		if graph[x][y] > 1{
			intersections++;
		}
	}}

	//get biggest number
	var biggest int;
	for x := 0; x < 1000; x++{
	for y := 0; y < 1000; y++{
		if graph[x][y] > biggest{
			biggest = graph[x][y];
		}
	}}
	var step = (255-biggest)/biggest;
	//make a nice image
	img := image.NewNRGBA(image.Rect(0, 0, 1000, 1000))
	for y := 0; y < 1000; y++ {
	for x := 0; x < 1000; x++ {
		img.Set(x, y, color.NRGBA{
			R: uint8((biggest+(step*graph[y][x]))*3%256),
			G: uint8((biggest+(step*graph[y][x]))*13%256),
			B: uint8((biggest+(step*graph[y][x]))*11%256),
			A: 255,
		})
	}}

	f, err := os.Create("image.png")
	if err != nil {
		log.Fatal(err)
	}

	if err := png.Encode(f, img); err != nil {
		f.Close()
		log.Fatal(err)
	}

	if err := f.Close(); err != nil {
		log.Fatal(err)
	}

	//print the number of intersections
	fmt.Println(intersections);
}