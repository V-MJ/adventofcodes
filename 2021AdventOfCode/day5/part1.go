package main

import ("fmt"
	"os"
	"bufio"
	"strconv"
	"strings"
)

type line struct {
	x1 int64;
	y1 int64;
	x2 int64;
	y2 int64;
}
var allLines []line;
var udLines []line;
var lrLines []line

func main(){
	//usefull variables
	scanner := bufio.NewScanner(os.Stdin);
	for scanner.Scan() {
		//parse input
		str := scanner.Text();
		str = strings.Replace(str, " -> ", ",", -1)
		strs := strings.Split(str,",");

		//get the lines
		var tmpLine line;
		
		tmpLine.x1, _ = strconv.ParseInt(strs[0], 10, 32);
		tmpLine.y1, _ = strconv.ParseInt(strs[1], 10, 32);
		tmpLine.x2, _ = strconv.ParseInt(strs[2], 10, 32);
		tmpLine.y2, _ = strconv.ParseInt(strs[3], 10, 32);
		allLines = append(allLines, tmpLine);
	}
	//fmt.Println(allLines);
	//make list of useable lines lines
	for lineNum := range allLines {
		if allLines[lineNum].x1 == allLines[lineNum].x2{
			udLines = append(udLines, allLines[lineNum]);
		}
		if allLines[lineNum].y1 == allLines[lineNum].y2{
			lrLines = append(lrLines, allLines[lineNum]);
		}
	}
	//draw lines to graph
	var graph [1000][1000] int;
	for cLine := range udLines{
		startY := udLines[cLine].y1;
		lenL := udLines[cLine].y2 - udLines[cLine].y1;
		//draw lines to graph
		if udLines[cLine].y1 > udLines[cLine].y2 {
			startY = udLines[cLine].y2;
			lenL = udLines[cLine].y1 - udLines[cLine].y2;
		}
		for y := startY; y != startY+lenL+1; y++ {
			graph[y][udLines[cLine].x1]++;
		}
	}
	
	for cLine := range lrLines{
		startY :=lrLines[cLine].x1;
		lenL := lrLines[cLine].x2 - lrLines[cLine].x1;
		//draw lines to graph
		if lrLines[cLine].x1 > lrLines[cLine].x2 {
			startY = lrLines[cLine].x2;
			lenL = lrLines[cLine].x1 - lrLines[cLine].x2;
		}
		for y := startY; y != startY+lenL+1; y++ {
			graph[lrLines[cLine].y1][y]++;
		}
	}




	//get the intersections
	var intersections int;
	for x := 0; x < 1000; x++{
	for y := 0; y < 1000; y++{
		if graph[x][y] > 1{
			//intersections += graph[x][y]-1;
			intersections++;
		}
	}}

	//print for good luck
	for x := 0; x < 1000; x++{
		for y := 0; y < 1000; y++{
			if graph[x][y] != 0{
			fmt.Printf("%d", graph[x][y]);
			} else{
				fmt.Printf(".");
			}
		}
		fmt.Printf("\n");
	}

	//print the number of intersections
	fmt.Println(intersections);
}