package main
import ("fmt"
	"os"
	"bufio"
	"strconv"
	"strings"
)

func main(){
	//usefull variables
	var crabs []int64;
	var smallest int64;
	var biggest int64;
	//get and split and set the input
	scanner := bufio.NewScanner(os.Stdin);
	scanner.Scan();
	strs := strings.Split(scanner.Text(),",");
	smallest, _ = strconv.ParseInt(strs[0], 10, 32);
	biggest, _ = strconv.ParseInt(strs[0], 10, 32);
	for strp := range strs{
		tmpInt, _ := strconv.ParseInt(strs[strp], 10, 32);
		if tmpInt < smallest{
			smallest = tmpInt;
		}
		if tmpInt > biggest {
			biggest = tmpInt;
		}
		crabs = append(crabs, tmpInt);
	}
	fmt.Println("nums", smallest, biggest);
	var leastFuel int64;
	var smallestPos int64;
	for pos := smallest; pos < biggest+1; pos++{
		var tmpFuel int64;
		for hcrab := range crabs{
			etäisyys := crabs[hcrab] - pos;
			if etäisyys < 0{
				etäisyys *= -1;
			}
			var cfuel int64;
			for i := int64(0); i < etäisyys+1; i++{
				cfuel += i;
			}
			tmpFuel += cfuel;
		}
		if pos == smallest{
			leastFuel = tmpFuel;
			smallestPos = pos;
		} else if tmpFuel < leastFuel{
			leastFuel = tmpFuel;
			smallestPos = pos;
		}
	}
	fmt.Println(leastFuel, smallestPos);
}