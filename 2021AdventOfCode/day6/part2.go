package main
import ("fmt"
	"os"
	"bufio"
	"strconv"
	"strings"
)

func main(){
	//usefull variables
	var fishes []int64;
	//get and split and set the input
	scanner := bufio.NewScanner(os.Stdin);
	scanner.Scan();
	strs := strings.Split(scanner.Text(),",");
	for strp := range strs{
		tmpInt, _ := strconv.ParseInt(strs[strp], 10, 32);
		fishes = append(fishes, tmpInt);
	}
	//fmt.Println(fishes);
	var fishButLess [9]int64;
	//put the fish in a datastructure
	for f := range fishes{
		fishButLess[fishes[f]]++; 
	}

	//do the calculation
	for i := 0; i < 256; i++{
		fishButShadow := fishButLess;
		fishButLess[8] = fishButShadow[0];
		fishButLess[7] = fishButShadow[8];
		fishButLess[6] = fishButShadow[7] + fishButShadow[0];
		fishButLess[5] = fishButShadow[6];
		fishButLess[4] = fishButShadow[5];
		fishButLess[3] = fishButShadow[4];
		fishButLess[2] = fishButShadow[3];
		fishButLess[1] = fishButShadow[2];
		fishButLess[0] = fishButShadow[1];
		fmt.Println("day:", i+1, "fishes:", fishButLess[0]+fishButLess[1]+fishButLess[2]+fishButLess[3]+fishButLess[4]+fishButLess[5]+fishButLess[6]+fishButLess[7]+fishButLess[8]);
	}
}