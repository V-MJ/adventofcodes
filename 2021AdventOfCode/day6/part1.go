package main
import ("fmt"
	"os"
	"bufio"
	"strconv"
	"strings"
)

func main(){
	//usefull variables
	var fishes []int64;
	//get and split and set the input
	scanner := bufio.NewScanner(os.Stdin);
	scanner.Scan();
	strs := strings.Split(scanner.Text(),",");
	for strp := range strs{
		tmpInt, _ := strconv.ParseInt(strs[strp], 10, 32);
		fishes = append(fishes, tmpInt);
	}
	//fmt.Println(fishes);
	//do the calculation
	for i := 0; i < 80; i++{
		for fp := range fishes{
			if fishes[fp] == 0{
				fishes[fp] = 6;
				fishes = append(fishes, 8);
			}else{
				fishes[fp]--;
			}
		}
		fmt.Println("day:", i+1, "fishes:", len(fishes));
	}
}