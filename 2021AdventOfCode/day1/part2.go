package main

import ("fmt"
	"os"
	"strconv"
	"bufio"
	"strings"
)

func main(){
	reader := bufio.NewReader(os.Stdin);
	text, _ := reader.ReadString('\n');
	text = strings.Replace(text, "\n", "", -1);
	prev0, _ := strconv.ParseInt(text, 10, 32);

	text, _ = reader.ReadString('\n');
	text = strings.Replace(text, "\n", "", -1);
	prev1, _ := strconv.ParseInt(text, 10, 32);

	text, _ = reader.ReadString('\n');
	text = strings.Replace(text, "\n", "", -1);
	prev2, _ := strconv.ParseInt(text, 10, 32);


	var increases int;
	var stop int = 0;
	for stop == 0 {
		text, err := reader.ReadString('\n');
		text = strings.Replace(text, "\n", "", -1);
		cur2, _ := strconv.ParseInt(text, 10, 32);
		cur1	:= prev2
		cur0	:= prev1

		cur := cur0 + cur1 + cur2;
		prev := prev0 + prev1 + prev2;

		if cur > prev {
			increases++;
		}
		if err != nil {
			stop++;
			continue;
		}

		prev0 = cur0;
		prev1 = cur1;
		prev2 = cur2;


	}
	fmt.Println(increases);
}