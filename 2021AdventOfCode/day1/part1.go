package main

import ("fmt"
	"os"
	"strconv"
	"bufio"
	"strings"
)

func main(){
	reader := bufio.NewReader(os.Stdin);
	text, _ := reader.ReadString('\n');
	text = strings.Replace(text, "\n", "", -1);
	prev, _ := strconv.ParseInt(text, 10, 32);
	
	fmt.Println(text, ", ", prev);

	var increases int;
	var stop int = 0;
	for stop == 0 {
		text, err := reader.ReadString('\n');
		text = strings.Replace(text, "\n", "", -1);
		cur, _ := strconv.ParseInt(text, 10, 32);
		if err != nil {
			stop++;
			continue;
		}

		if cur > prev {
			increases++;
		}
		fmt.Println(text, ", ", prev);
		prev = cur;

	}
	fmt.Println(increases);
}