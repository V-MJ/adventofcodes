package main
import ("fmt"
	"os"
	"bufio"
)
func main(){
	//get and split the input
	scanner := bufio.NewScanner(os.Stdin);
	var errorChars []byte;
	for scanner.Scan(){
		line := scanner.Text();
		var starters []byte;
		for i := 0; i<len(line); i++{
			if line[i] == '{' {
				starters = append(starters, '}');
			}
			if line[i] == '(' {
				starters = append(starters, ')');
			}
			if line[i] == '[' {
				starters = append(starters, ']');
			}
			if line[i] == '<' {
				starters = append(starters, '>');
			}

			tmp := line[len(starters)-1]

			if line[i] == tmp{
				continue;
			}

			if line[i] != tmp{
				if 
				errorChars = append(errorChars, line[i]);
				break;
			}
		}
		fmt.Println(string(errorChars));
	}
}