package main

import ("fmt"
	"os"
	"bufio"
	"strconv"
)
//3549854, 3765399 thanks kind anon
func main(){
	var gamma1 [12]int;
	var strins []string;
	var ogr int64;
	var csr int64;

	//get input
	scanner := bufio.NewScanner(os.Stdin)
	for scanner.Scan() {
		str := scanner.Text();
		strins = append(strins, str);
		for i := 0; i < 12; i++{
			var num int = int(str[i]) - 48;
			if num == 1 {
				gamma1[i]++;
			}
		}
	}

	oxy := strins;
	for i := 0; i < 12; i++ {
		var tmp []string;
		//get the "rarest type of remainig options"
		var rarer int;
		for s := range oxy{
			if oxy[s][i] == '1'{
				rarer++;
			}
		}
		if len(oxy)-rarer <= rarer{
			rarer = 0;
		} else {
			rarer = 1;
		}

		for s := range oxy {
			if rarer == 1 {
				if oxy[s][i] == '1'{
					tmp = append(tmp, oxy[s]);
				}
			} else {
				if oxy[s][i] == '0'{
					tmp = append(tmp, oxy[s]);
				}
			}
		}
		oxy = tmp;
		if len(oxy) == 1{
			break;
		}
	}
	ogr, _ = strconv.ParseInt(oxy[0], 2, 32);

	//process co2 ratings
	co2 := strins;
	for i := 0; i < 12; i++ {
		var tmp []string;
		//get the "rarest type of remainig options"
		var rarer int;
		for s := range co2{
			if co2[s][i] == '1'{
				rarer++;
			}
		}
		if len(co2)-rarer > rarer{
			rarer = 0;
		} else {
			rarer = 1;
		}

		for s := range co2 {
			if rarer == 1 {
				if co2[s][i] == '1'{
					tmp = append(tmp, co2[s]);
				}
			} else {
				if co2[s][i] == '0'{
					tmp = append(tmp, co2[s]);
				}
			}
		}
		co2 = tmp;
		if len(co2) == 1{
			break;
		}
	}
	csr, _ = strconv.ParseInt(co2[0], 2, 32);



	lsr := ogr * csr; 
	fmt.Println(lsr, ogr, csr, oxy, co2, gamma1);
}