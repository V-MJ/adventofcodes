package main

import ("fmt"
	"os"
	"bufio"
)
//3549854
//3765399
func main(){
	var gamma1 [12]int;
	var gamma0 [12]int;

	var lc int;

	//get input
	scanner := bufio.NewScanner(os.Stdin)
	for scanner.Scan() {
		str := scanner.Text();
		for i := 0; i < 12; i++{
			var num int = int(str[i]) - 48;
			if num == 1 {
				gamma1[i]++;
			} else {
				gamma0[i]++;
			}
			fmt.Printf("%d",num);
		}
		fmt.Printf("\n");
		lc++;
	}
	//process input
	var gammaT uint16 = 0;
	var eT uint16 = 0;
	for i := 0; i < 12; i++{
		if gamma1[i] > gamma0[i]{
			gammaT = gammaT<<1;
			gammaT++;
			eT = eT<<1;
		} else {
			gammaT = gammaT<<1;
			eT = eT<<1
			eT++;
		}
	}
	var an uint64 = uint64(gammaT) * uint64(eT); 
	fmt.Println(gammaT, eT, gammaT^eT, int64(gammaT*eT), an, lc);
}